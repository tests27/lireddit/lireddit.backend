import { EntityManager, IDatabaseDriver, Connection } from "@mikro-orm/core";
import { Request, Response } from "express";

export type MyContext = {
  req: Request & { session: Express.Session };
  res: Response;
  em: EntityManager<any> & EntityManager<IDatabaseDriver<Connection>>;
};
